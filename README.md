For more information on Project creation, visit my personal wiki at https://wiki.dalada.de/doku.php?id=projekte:feuerwerksautomat

To build the project you need the Scons build environment and the gcc-arm-linux-gnueabi compiler suite. Additionally I used the pigpio library for accessing Raspberry Pi IO.
The SConstruct file assumes to have the pigpio library installed in ../../tools/pigpio

Following build targets are enabled:
scons LocalDebug    -- creates a build target for the local machine to do little debugging; Pigpio is stubbed in this target to just do console prints;
scons LocalTest     -- creates a build target for the local machine, for testing purpose; Pigpio is stubbed and additionally a test env is loaded (not implemented)
scons RemoteRun     -- creates a build target for the target machine (Raspberry Pi 3B); Pigpio is included, no test env is loaded

So for compiling for a test run on the Pi, go to 'cd ./Software' and call 'scons RemoteRun'. Afterwards move the target '.RemoteRun/fip' to the Pi and start it 
with root privileges (the pigpio library needs root privileds) 'sudo path/to/fip'

You will see a launch menu which should be self explaning: Simply press the keys in brackets and the software will tell you what is done or what has to be done.

Currently you can select one of 4 Sequences (A, B, C, T), where T is a test sequence. But actually all of the sequences are some kind of test sequences and there is not 
big difference. The sequences are hard coded into 'sequence.c'

An extensive log is created into fip.log on calling path.

The software is not very flexible, this is the reason why the complete project will be restarted with some big differences, but leading to a more flexible approach.

