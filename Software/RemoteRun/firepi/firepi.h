#ifndef __FIREPI_H__
#define __FIREPI_H__

#include "firepi_cfg.h"

#define FIP_ERR_WRONG_INIT 0
#define FIP_ERR_NOT_ALLOWED 1
#define FIP_ERR_WRONG_PARAS 2
#define FIP_ERR_WRONG_STATE 5
#define FIP_ERR_EXT_MOD_ERR 99

void FIP_AllowArm(void);
void FIP_DisallowArm(void);

typedef enum FIP_ArmAllowedStates {
  FIP_ARM_NOT_ALLOWED,
  FIP_ARM_ALLOWED = 0xAC
} FIP_ArmAllow_t;

void FIP_init(FIP_ArmAllow_t (*func_ArmAllowed)(void),
              void (*func_ErrorHandler)(int errid));

int FIP_firePulse(int pulseActiveTime, int pulseActiveChanels[],
                  int pulseChannelCount);

int FIP_immediate_STOP(void);

int FIP_error();

#endif /*__FIREPI_H__*/
