#include "firepi.h"

// TODO: Make ExclusiveAreas smaller to allow faster reactions
// TODO: Implement ExclusiveAreas
// TODO: Make FIP_wait active waiting and interruptable by FIP_immediate_STOP
// function
// TODO: Implement FIP_immediate_STOP function

#include "logger.h"
#include "pigpio.h"
#include "pthread.h"
#include "stdlib.h"

/***************************************************************
** Local Defines
**********************************/

#define LVL_HIGH 0x01
#define LVL_LOW 0x00

#define FIP_MAX_CHANNEL_BYTES (FIP_MAX_CHANNELS / 8)

#define FIP_SET_ERROR(errid) (FIP_currentState = FIP_ERROR - errid)

/***************************************************************
** Local Type Definitions
**********************************/

typedef enum FIP_States {
  FIP_ERROR = -1,
  FIP_INIT = 1,
  FIP_IDLE = 2,
  FIP_PROGRAM = 3,
  FIP_PROGRAMMED = 5,
  FIP_ARMED = 9
} FIP_States_t;

/***************************************************************
** Global Variables
**********************************/

/***************************************************************
** Local global Variables
**********************************/

int FIP_currentState;
unsigned char FIP_nextPulse[FIP_MAX_CHANNEL_BYTES];
int FIP_pulseActiveTime;
volatile FIP_ArmAllow_t FIP_armAllowed;

pthread_mutex_t FIP_Mutex_PULSE = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t FIP_Mutex_ALLOWARM = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t FIP_Mutex_ALTERSTATE = PTHREAD_MUTEX_INITIALIZER;

/***************************************************************
** Local Function Definitions
**********************************/

void FIP_SetState(FIP_States_t newState);

void FIP_SetError(int error);

int FIP_doProgram();
int FIP_doFire();

int FIP_arm();
int FIP_unarm();

FIP_ArmAllow_t FIP_intAllowArm(void);

FIP_ArmAllow_t (*FIP_getArmAllowed)(void);
void (*FIP_extErrorHandler)(int errid);

int FIP_CheckArmAllowed();

void FIP_EA_PULSE_enter();
void FIP_EA_PULSE_leave();
void FIP_EA_ALLOWARM_enter();
void FIP_EA_ALLOWARM_leave();
void FIP_EA_ALTERSTATE_enter();
void FIP_EA_ALTERSTATE_leave();

void FIP_wait(int milliseconds);

int FIP_Gpio_Abs(int func_ret);

/***************************************************************
** Global Function Implementations
**********************************/

void FIP_init(FIP_ArmAllow_t (*func_ArmAllowed)(void),
              void (*func_ErrorHandler)(int errid)) {
  LOGDEBUG("FIP_init called");

  FIP_EA_PULSE_enter();

  FIP_SetState(FIP_INIT);

  // TODO Initialize local Variables
  //  - AllowArm
  //  - pulseActiveTime
  //  - pulseChannelCount
  //  - ...

  if (NULL == func_ArmAllowed) {
    FIP_getArmAllowed = FIP_intAllowArm;
    LOGDEBUG("ArmAllowed getter function set to internal");
  } else {
    FIP_getArmAllowed = func_ArmAllowed;
    LOGDEBUG("ArmAllowed getter function set to external");
  }

  FIP_extErrorHandler = func_ErrorHandler;

  LOGDEBUG("FIP_init: Initialize pgpio library");
  if (gpioInitialise() < 0) {
    FIP_SetError(FIP_ERR_EXT_MOD_ERR);
    LOGERROR("Initializing external libriary pigpio returned error. Return ");
    return;
  }

  /* Init GIPO */
  LOGDEBUG("FIP_init: Set all PINs to output");
  FIP_Gpio_Abs(gpioSetMode(FIP_PIN_SHIFT_STAGING, PI_OUTPUT));
  FIP_Gpio_Abs(gpioSetMode(FIP_PIN_SHIFT_DATA, PI_OUTPUT));
  FIP_Gpio_Abs(gpioSetMode(FIP_PIN_SHIFT_CLK, PI_OUTPUT));
  FIP_Gpio_Abs(gpioSetMode(FIP_PIN_FIRE, PI_OUTPUT));
  FIP_Gpio_Abs(gpioSetMode(FIP_PIN_PROGEN, PI_OUTPUT));
  FIP_Gpio_Abs(gpioSetMode(FIP_PIN_FIREEN, PI_OUTPUT));

  LOGDEBUG("FIP_init: Set all PINs to low (0)");
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_LOW));
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_DATA, LVL_LOW));
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_CLK, LVL_LOW));
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_FIRE, LVL_LOW));
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_PROGEN, LVL_LOW));
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_FIREEN, LVL_LOW));
  gpioDelay(1);

  FIP_SetState(FIP_IDLE);

  FIP_EA_PULSE_leave();
}

void FIP_SetState(FIP_States_t newState) {
  FIP_EA_ALTERSTATE_enter();

  if (FIP_currentState <= FIP_ERROR) {
    return;
  }

  switch (newState) {
  case FIP_INIT:
    if (FIP_currentState == FIP_ARMED) {
      FIP_unarm();
      FIP_SET_ERROR(FIP_ERR_WRONG_STATE);
    }
    FIP_currentState = FIP_INIT;
    LOGDEBUG("FIP Current State = INIT");
    break;
  case FIP_IDLE:
    // TODO: check current state for INIT or UNARM
    FIP_currentState = FIP_IDLE;
    LOGDEBUG("FIP Current State = FIP_IDLE");
    break;
  case FIP_PROGRAM:
    if (FIP_currentState != FIP_IDLE) {
      FIP_SET_ERROR(FIP_ERR_WRONG_STATE);
      break;
    }
    FIP_currentState = FIP_PROGRAM;
    LOGDEBUG("FIP Current State = FIP_PROGRAM");
    break;
  case FIP_PROGRAMMED:
    if (FIP_currentState != FIP_PROGRAM) {
      FIP_SET_ERROR(FIP_ERR_WRONG_STATE);
      break;
    }
    FIP_currentState = FIP_PROGRAMMED;
    LOGDEBUG("FIP Current State = FIP_PROGRAMMED");
    break;
  case FIP_ARMED:
    // TODO:shall we check a state here? What is more safe?
    FIP_currentState = FIP_ARMED;
    break;
  default:
    if (newState > FIP_ERROR) {
      LOGDEBUG("FIP State switch not possible (%d -> %d)", FIP_currentState,
               newState);
      FIP_SET_ERROR(FIP_ERR_WRONG_STATE);
    } else {
      FIP_currentState = newState;
    }
    break;
  }

  FIP_EA_ALTERSTATE_leave();
}

int FIP_CheckState(FIP_States_t checkState) {
  int retval;

  FIP_EA_ALTERSTATE_enter();

  retval = (FIP_currentState == checkState);

  FIP_EA_ALTERSTATE_leave();

  LOGDEBUG("FIP_CheckState(%d) returns '%s', current State is %d", checkState,
           (retval) ? "ExpectedState" : "UnexpectedState", FIP_currentState);

  if (!retval) {
    FIP_SetError(FIP_ERR_WRONG_STATE);
  }

  return retval;
}

void FIP_SetError(int error) { FIP_SetState(FIP_ERROR - error); }

int FIP_CheckError() {
  int retval;

  FIP_EA_ALTERSTATE_enter();

  retval = (FIP_currentState <= FIP_ERROR);

  FIP_EA_ALTERSTATE_leave();
  LOGDEBUG("FIP_CheckError returns '%s'", (retval) ? "ErrorActive" : "NoError");

  return retval;
}

int FIP_firePulse(int pulseActiveTime, int pulseActiveChannels[],
                  int pulseChannelCount) {

  LOGDEBUG("FIP_firePulse called");

  if (!FIP_CheckState(FIP_IDLE)) {
    return -1;
  }

  FIP_EA_PULSE_enter();

  if (pulseActiveTime <= 0 || pulseActiveChannels == 0 ||
      pulseChannelCount < 0) {
    FIP_SetError(FIP_ERR_WRONG_PARAS);
    pulseChannelCount = 0;
  }

  for (int chan = 0; chan < pulseChannelCount; chan++) {
    if (pulseActiveChannels[chan] < 0) {
      continue;
    }

    LOGDEBUG("FIP FIP_nextPulse[%d].%d => 1", pulseActiveChannels[chan] / 8,
             pulseActiveChannels[chan] % 8);
    FIP_nextPulse[pulseActiveChannels[chan] / 8] |=
        (1 << (pulseActiveChannels[chan] % 8));
  }

  FIP_SetState(FIP_PROGRAM);

  FIP_doProgram();

  FIP_arm();

  FIP_doFire(pulseActiveTime);

  FIP_unarm();

  FIP_EA_PULSE_leave();

  if (FIP_CheckError()) {
    return -1;
  }

  return 0;
}

/***************************************************************
** Local Function Implementations
**********************************/

int FIP_doProgram() {
  LOGDEBUG("FIP_doProgram called");

  if (!FIP_CheckState(FIP_PROGRAM)) {
    return -1;
  }

  for (int i = FIP_MAX_CHANNELS - 1; i >= 0; i--) {
    // TODO: Wrap gpiocalls with error recognition
    LOGDEBUG("FIP write pulse Channel[%d].%d => %d", i / 8, i % 8,
             (FIP_nextPulse[i / 8] >> (i % 8)) & 1);
    FIP_Gpio_Abs(
        gpioWrite(FIP_PIN_SHIFT_DATA, (FIP_nextPulse[i / 8] >> (i % 8)) & 1));
    gpioDelay(1);
    FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_CLK, LVL_HIGH));
    gpioDelay(1);
    FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_CLK, LVL_LOW));
    gpioDelay(1);
    FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_DATA, 0));
    gpioDelay(1);
  }

  // clear next pulse field
  for (int i = 0; i < FIP_MAX_CHANNEL_BYTES; i++) {
    FIP_nextPulse[i] = 0;
  }

  FIP_Gpio_Abs(gpioWrite(FIP_PIN_PROGEN, LVL_HIGH));
  FIP_SetState(FIP_PROGRAMMED);
}

/** FIP_arm()
*   - Sets the Shift registers into a ready state and enables the firing logic.
*     Finally FIP_currentState is set to FIP_ARMED
*   returns: error-flag - Set in case of an error. See FIP_currentState for more
* Details
*/
int FIP_arm() {
  LOGDEBUG("FIP_arm called");

  if (!FIP_CheckState(FIP_PROGRAMMED)) {
    return -1;
  }

  if (!FIP_CheckArmAllowed()) {
    return -1;
  }

  // TODO: Wrap gpiocalls with error recognition
  // Stage outputs
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_LOW));
  gpioDelay(1);
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_HIGH));
  gpioDelay(1);
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_LOW));
  gpioDelay(1);

  // TODO: Wrap gpiocalls with error recognition
  // set ProgDIS and FireEN
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_PROGEN, LVL_LOW));
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_FIREEN, LVL_HIGH));

  FIP_SetState(FIP_ARMED);

  return 0;
}

/** FIP_doFire()
*   - Gives the firing pulse which opens the relais. The pulse shall be active
for a configurable
      amount, but returns after the pulse has finished
*   returns: error-flag - Set in case of an error. See FIP_currentState for more
Details
*/
int FIP_doFire(int pulseActiveTime) {
  LOGDEBUG("FIP_doFire called");

  if (!FIP_CheckState(FIP_ARMED)) {
    return -1;
  }

  if (!FIP_CheckArmAllowed()) {
    return -1;
  }

  // TODO: Wrap gpiocalls with error recognition
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_FIRE, LVL_LOW));
  gpioDelay(1);
  FIP_Gpio_Abs(gpioWrite(FIP_PIN_FIRE, LVL_HIGH));

  // TODO should be interruptable, active wait, to get rid of dependency and to
  // be able to
  // stop firing in worst case immediatelly
  FIP_wait(pulseActiveTime);

  FIP_Gpio_Abs(gpioWrite(FIP_PIN_FIRE, LVL_LOW));

  return 0;
}

/** FIP_unarm()
*   - First of all, firing logic is disabled, afterwards the shift registers are
cleared
      Finally FIP_currentState is set to FIP_IDLE
*   returns: error-flag - Set in case of an error. See FIP_currentState for more
Details
*/
int FIP_unarm() {
  LOGDEBUG("FIP_unarm called");

  // set ProgEN/FireDIS
  gpioWrite(FIP_PIN_FIREEN, LVL_LOW);
  gpioWrite(FIP_PIN_PROGEN, LVL_HIGH);
  // clear last sequence

  gpioWrite(FIP_PIN_SHIFT_DATA, 0);

  for (int i = 0; i < FIP_MAX_CHANNELS; i++) {
    gpioWrite(FIP_PIN_SHIFT_CLK, LVL_HIGH);
    gpioDelay(1);
    gpioWrite(FIP_PIN_SHIFT_CLK, LVL_LOW);
  }

  gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_LOW);
  gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_HIGH);
  gpioDelay(1);
  gpioWrite(FIP_PIN_SHIFT_STAGING, LVL_LOW);

  gpioWrite(FIP_PIN_PROGEN, LVL_LOW);

  FIP_SetState(FIP_IDLE);

  return 0;
}

void FIP_AllowArm(void) {
  FIP_EA_ALLOWARM_enter();

  FIP_armAllowed = FIP_ARM_ALLOWED;

  FIP_EA_ALLOWARM_leave();
}

void FIP_DisallowArm(void) {
  FIP_EA_ALLOWARM_enter();

  FIP_armAllowed = FIP_ARM_NOT_ALLOWED;

  FIP_EA_ALLOWARM_leave();
}

FIP_ArmAllow_t FIP_intAllowArm() {
  int intAllowed;

  FIP_EA_ALLOWARM_enter();

  intAllowed = (FIP_armAllowed);

  FIP_EA_ALLOWARM_leave();

  LOGDEBUG("FIP ArmAllowed %s",
           (intAllowed == FIP_ARM_ALLOWED) ? "FIP_ARM_ALLOWED"
                                           : "FIP_ARM_NOT_ALLOWED");

  return intAllowed;
}

int FIP_CheckArmAllowed() {
  int retVal;

  retVal = (FIP_getArmAllowed() == FIP_ARM_ALLOWED);

  if (!retVal) {
    FIP_SetError(FIP_ERR_NOT_ALLOWED);
  }

  return retVal;
}

int FIP_immediate_STOP() { FIP_unarm(); }

void FIP_wait(int milliseconds) {
  // should be interruptable, active wait, to get rid of dependency and to be
  // able to
  // stop firing in worst case immediatelly
  FIP_Gpio_Abs(
      gpioSleep(PI_TIME_RELATIVE, milliseconds / 1000,
                (milliseconds % 1000) * 1000)); /*wait for pulseActiveTime ms*/
}

void FIP_EA_PULSE_enter() { pthread_mutex_lock(&FIP_Mutex_PULSE); }

void FIP_EA_PULSE_leave() { pthread_mutex_unlock(&FIP_Mutex_PULSE); }

void FIP_EA_ALLOWARM_enter() { pthread_mutex_lock(&FIP_Mutex_ALLOWARM); }

void FIP_EA_ALLOWARM_leave() { pthread_mutex_unlock(&FIP_Mutex_ALLOWARM); }

void FIP_EA_ALTERSTATE_enter() { pthread_mutex_lock(&FIP_Mutex_ALTERSTATE); }

void FIP_EA_ALTERSTATE_leave() { pthread_mutex_unlock(&FIP_Mutex_ALTERSTATE); }

int FIP_Gpio_Abs(int func_ret) {
  if (func_ret != 0) {
    FIP_SET_ERROR(FIP_ERR_EXT_MOD_ERR);
    LOGERROR("Calling external GPIO function returned error. Return value: %d",
             func_ret);
    return -1;
  }
  return 0;
}
