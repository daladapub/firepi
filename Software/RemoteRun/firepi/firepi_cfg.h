#ifndef __FIREPI_CFG_H__
#define __FIREPI_CFG_H__

#define FIP_PIN_SHIFT_STAGING 27
#define FIP_PIN_SHIFT_DATA 22
#define FIP_PIN_SHIFT_CLK 23
#define FIP_PIN_FIRE 26

#define FIP_PIN_PROGEN 5
#define FIP_PIN_FIREEN 13

#define FIP_MAX_CHANNELS 16 /*16 Channels*/

#define FIP_FIREACTIVE 20 /*20ms*/

#endif /*__FIREPI_CFG_H__*/
