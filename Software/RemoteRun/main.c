#include "console.h"
#include "errno.h"
#include "error.h"
#include "firepi.h"
#include "logger.h"
#include "pthread.h"
#include "sequences.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "sys/time.h"
#include "time.h"
#include "unistd.h"

typedef struct firepulseargs_s {
  int pulseActiveTime;
  int *pulseActiveChannels;
  int pulseChannelCount;
} firepulseargs_t;

FILE *logfile;

void Init() {
  if (!(logfile = fopen("fip.log", "w"))) {
    perror("ERROR opening log file fip.log");
    exit(1);
  }
}

void *fip_firepulse_enc(void *ptr) {
  FIP_firePulse((int)((firepulseargs_t *)ptr)->pulseActiveTime,
                (int *)((firepulseargs_t *)ptr)->pulseActiveChannels,
                (int)((firepulseargs_t *)ptr)->pulseChannelCount);

  pthread_exit((void *)pthread_self());
}

void *handleIO(void *ptr) {
  do {
    // Three buttons:
    // Button S for selecting next sequence from A to H and T
    // If T is selected, Button A has to be pushed for 1s
    // If A to H is selected, Buttons A and B have to be pushed for 5s
    // If any of the buttons is pushed, any sequence is stopped
    // The currently active Sequence shall be shown on display
    // If the sequence is active, it shall be blinking in 1s base

  } while (!getExitFlag());
}

int64_t getCurrentTimeMS() {
  struct timeval curTime;

  if (gettimeofday(&curTime, NULL) == -1) {
    return -1;
  }

  return (curTime.tv_sec * 1000) + (curTime.tv_usec / 1000);
}

int64_t getTimeDiffMS(int64_t time1ms, int64_t time2ms) {
  return (time2ms - time1ms);
}

int sleepTimeMS(int64_t milliseconds, int64_t maxtime) {
  if (maxtime < milliseconds)
    milliseconds = maxtime;
  struct timespec sleeptime;
  sleeptime.tv_sec = milliseconds / 1000;
  sleeptime.tv_nsec = (milliseconds % 1000) * 1000000;

  return (nanosleep(&sleeptime, NULL));
}

int main(int argc, char const *argv[]) {
  Sequence_t *loadedSequenceConfPtr = NULL;
  int activeSequence;
  bool exitCondition, pulseStopCondition;
  pthread_t firepulseth, consoleinpth;
  int64_t timeDiff = 0;
  int nextEvent = 0;
  int fipthreadstate = 0;
  bool pulseActive = false;
  struct timespec fipwaittime = {0, 10000000};
  firepulseargs_t fipargs;
  fipargs.pulseActiveTime = 1000;
  fipargs.pulseChannelCount = 20;
  Init();
  LOGDEBUG("Init finished");

  FIP_init(NULL, NULL);
  console_Init();

  pthread_create(&consoleinpth, NULL, console_inputController, NULL);

  LOGDEBUG("Starting endless loop");
  // loop for ever, until exit is written to console
  for (;;) {
    activeSequence = getActiveSequence();
    pulseStopCondition = getPulseStopFlag();
    exitCondition = getExitFlag();

    if (pulseStopCondition) {
      LOGDEBUG("main: Immediate Stop condition was set");
      activeSequence = NO_SEQUENCE;
      loadedSequenceConfPtr = NULL;
      nextEvent = 0;
      FIP_DisallowArm();
      FIP_immediate_STOP();
    }

    if (exitCondition) {
      activeSequence = NO_SEQUENCE;
      loadedSequenceConfPtr = NULL;
      nextEvent = 0;
      if (!pulseActive) {
        LOGDEBUG("main: Exit requested. Will shutdown when pulse is finished.");
        break;
      }
    }

    if (!pulseActive && loadedSequenceConfPtr == NULL) {
      // CASE 1: Wait for Sequence to load
      // No active pulse AND no yet loaded seqeunce
      if (activeSequence == NO_SEQUENCE) {
        // If there is no sequence to laod, wait for input
        LOGDEBUG("main: No active Sequence and no Sequence requested. Will "
                 "wait for input.");
        console_WaitInput();
      } else {
        // There is a sequence to load
        LOGDEBUG("main: New Sequence requested, loading %s",
                 SequenceNames[activeSequence]);
        loadedSequenceConfPtr = loadActiveSequence(activeSequence);
        nextEvent = 0;
      }
    } else if (!pulseActive && loadedSequenceConfPtr != NULL) {
      // CASE 2: Start each pulse of sequence
      // No active pulse, but a sequence was loaded before
      timeDiff =
          getTimeDiffMS(getCurrentTimeMS(),
                        loadedSequenceConfPtr->events[nextEvent].startTimeMS);

      console_setNextShotDueTime(timeDiff);
      // Check the current time vs the time of the first/next shot of the
      // sequence
      if (timeDiff <= 0) {
        LOGDEBUG("main: Next shot due. Will launch now.");
        fipargs.pulseActiveChannels =
            loadedSequenceConfPtr->events[nextEvent].channels;
        fipargs.pulseActiveTime =
            loadedSequenceConfPtr->events[nextEvent].activeTimeMS;
        setActiveSequenceStarted();
        FIP_AllowArm();
        // if a sequnce is due call sequence
        pthread_create(&firepulseth, NULL, fip_firepulse_enc, (void *)&fipargs);
        pulseActive = 1;

        nextEvent++;
        if (nextEvent < loadedSequenceConfPtr->events_count) {
          LOGDEBUG("main: The next pulse is due in %lld ms",
                   getTimeDiffMS(
                       getCurrentTimeMS(),
                       loadedSequenceConfPtr->events[nextEvent].startTimeMS));
        } else {
          LOGDEBUG("main: Last pulse of Sequence fired.");
          nextEvent = 0;
          loadedSequenceConfPtr = NULL;
        }
      } else {
        // wait for time difference, but for 100ms at max
        sleepTimeMS(timeDiff, 100);
      }
    } else /*if (pulseActive ) */ {
      // CASE 3: Wait for end of event
      // Pulse is active, try to join
      fipthreadstate = pthread_timedjoin_np(firepulseth, NULL, &fipwaittime);
      if (fipthreadstate == 0) {
        FIP_DisallowArm();
        pulseActive = 0;
        if (loadedSequenceConfPtr == NULL) {
          // CASE 4: End of last pulse of sequence
          LOGDEBUG("main: Last pulse of Sequence finished.");
          setActiveSequenceEnded();
        }
      }
    }
  }

  // pthread_join(firepulseth, NULL);
  pthread_join(consoleinpth, NULL);

  fclose(logfile);

  return 0;
}
