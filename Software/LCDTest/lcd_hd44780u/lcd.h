#ifndef __LCD_H__
#define __LCD_H__

void lcd_CursorHome();
void lcd_ClearScreen();
void lcd_SetDisplay(int state);
void lcd_SetCursor(int state);
void lcd_SetCursorBlink(int state);
void lcd_SendCommand(unsigned char command);
void lcd_SetPosition(int x, int y);
void lcd_DefCharDisp(int index, unsigned char data [8]);
void lcd_PutChar(unsigned char data);
void lcd_Puts(const char *string);
void lcd_Printf(const char *message, ...);

int  lcd_Init();


#endif /*__LCD_H__*/

