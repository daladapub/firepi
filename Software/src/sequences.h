#ifndef __SEQUENCES_H__
#define __SEQUENCES_H__

#include "stdint.h"
#include "unistd.h"

#define MIDNIGHT 1514761200000l
#define TIME_MS(t) (t * 1000)

// we need a buffer, that stores 0-based time values in future with according
// fire events (list of due channels max 4 at a time)
typedef struct {
  int64_t startTimeMS;
  int64_t activeTimeMS;
  int channels[20];
} Event_t;

typedef struct {
  Event_t events[20];
  int events_count;
  void (*dynamic_initializer)(void);
} Sequence_t;

enum Sequences {
  NO_SEQUENCE = 0,
  SEQUENCE_A,
  SEQUENCE_B,
  SEQUENCE_C,
  SEQUENCE_T
};

extern char SequenceShortNames[];
extern char SequenceNames[][12];

extern Sequence_t SequenceA, SequenceB, SequenceC, SequenceT;

#define SIZE_OF_EVENTS (sizeof(events) / sizeof(events[0]))

Sequence_t *loadActiveSequence(enum Sequences);

#endif /*__SEQUENCES_H__*/
