#include "pigpio.h"

int gpioInitialise(void) {
  return 0;
}

int gpioTick(void) {
  return 0;
}

int gpioSleep(unsigned timetype, int seconds, int micros) {
  return 0;
}

int gpioDelay(int micros) {
  return 0;
}

int gpioTime(unsigned timetype, int *seconds, int *micros) {
  return 0;
}

int gpioSetMode(unsigned gpio, unsigned mode) {
  return 0;
}

int gpioWrite(unsigned gpio, unsigned level) {
  return 0;
}
