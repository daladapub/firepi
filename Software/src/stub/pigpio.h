#ifndef __PIGPIO_H__
#  define __PIGPIO_H__

#define PI_OUTPUT 1
#define PI_TIME_RELATIVE  1

int gpioInitialise(void);

int gpioTick(void);
int gpioSleep(unsigned timetype, int seconds, int micros);
int gpioDelay(int micros);
int gpioTime(unsigned timetype, int *seconds, int *micros);

int gpioSetMode(unsigned gpio, unsigned mode);
int gpioWrite(unsigned gpio, unsigned level);

#endif /*__PIGPIO_H__*/
