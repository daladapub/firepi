#include "firepi.h"
#include "stdlib.h"
#include "stdio.h"

#include "test.h"




int main(int argc, char const *argv[]) {
  TestCase_t *curTC;
  char results[3][15] = { "TC_NOTRUN (0)", "TC_PASSED (1)", "TC_FAILED (2 )"};

  //run each of the test scenarios and output test state to console
  for(curTC = TestCases; curTC->id >= 0; curTC++) {
    printf("TestCase %d:\n", curTC->id);
    printf(">> %s\n", curTC->desc);
    printf(">> Result: %s\n", results[((*(curTC->testfunc))())]);
  }


  //wait until 24th of dec 24:00 and then start sequence

  return 0;
}
