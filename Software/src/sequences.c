#include "sequences.h"

#include "stdio.h"
#include "time.h"

char SequenceShortNames[] = "0ABCT";
char SequenceNames[][12] = {"No Sequence", "Sequence A", "Sequence B",
                            "Sequence C", "Sequence T"};

void initializeSequenceT(void);
void initializeSequenceB(void);
void initializeSequenceA(void);

int64_t getCurrentTimeMS();

Sequence_t SequenceA = {
    // Event_t[] events
    {},
    0,                  // int event_count
    initializeSequenceA // void (*dynamic_initialize)(void)
};

Sequence_t SequenceB = {
    {},
    0,
    initializeSequenceB // void (*dynamic_initialize)(void)
};

Sequence_t SequenceC = {
    {},
    0,
    NULL // void (*dynamic_initialize)(void)
};

Sequence_t SequenceT = {
    {},
    0,
    initializeSequenceT // void (*dynamic_initialize)(void)
};

Sequence_t *loadActiveSequence(enum Sequences sequence) {
  Sequence_t *retval;
  switch (sequence) {
  case SEQUENCE_A:
    retval = &SequenceA;
    break;
  case SEQUENCE_B:
    retval = &SequenceB;
    break;
  case SEQUENCE_C:
    retval = &SequenceC;
    break;
  case SEQUENCE_T:
    retval = &SequenceT;
    break;
  default:
    retval = NULL;
  }
  if (retval != NULL && retval->dynamic_initializer != NULL) {
    retval->dynamic_initializer();
  }
  return retval;
}

void initializeSequenceA(void) {
  int offsets[] = {/* 1:*/ 0,
                   /* 2:*/ 2000,
                   /* 3:*/ 4000,
                   /* 4:*/ 7000,
                   /* 5:*/ 9000,
                   /* 6:*/ 11000,
                   /* 7:*/ 15000,
                   /* 8:*/ 17000,
                   /* 9:*/ 19000,
                   /*10:*/ 22000,
                   /*11:*/ 25000,
                   /*12:*/ 27000,
                   /*13:*/ 28000,
                   /*14:*/ 31000,
                   /*15:*/ 35000,
                   /*16:*/ 39000};

  int activations[] = {/* 1:*/ 1000,
                       /* 2:*/ 1000,
                       /* 3:*/ 1000,
                       /* 4:*/ 1000,
                       /* 5:*/ 1000,
                       /* 6:*/ 1000,
                       /* 7:*/ 1000,
                       /* 8:*/ 1000,
                       /* 9:*/ 1000,
                       /*10:*/ 1000,
                       /*11:*/ 1000,
                       /*12:*/ 1000,
                       /*13:*/ 1000,
                       /*14:*/ 1000,
                       /*15:*/ 1000,
                       /*16:*/ 1000};

  int64_t startTime = 0;
  struct tm tmVar;
  time_t timeVar;

  strptime("2018-01-01 00:14:57", "%Y-%m-%d %H:%M:%S", &tmVar);

  startTime = (mktime(&tmVar) * 1000);
  printf("Silvester ist in %lld ms\n", startTime);

  for (int i = 0; i < 16; i++) {
    SequenceA.events[i].channels[0] = i;

    for (int j = 1; j < 20; j++) {
      SequenceA.events[i].channels[j] = -1;
    }
    SequenceA.events[i].startTimeMS = startTime + offsets[i];
    SequenceA.events[i].activeTimeMS = activations[i];
  }
  SequenceA.events_count = 16;
}

void initializeSequenceT(void) {
  int64_t startTime = getCurrentTimeMS() + 2000;

  for (int i = 0; i < 16; i++) {
    SequenceT.events[i].channels[0] = i;
    for (int j = 1; j < 20; j++) {
      SequenceT.events[i].channels[j] = -1;
    }
    SequenceT.events[i].startTimeMS = startTime + (i * 2000);
    SequenceT.events[i].activeTimeMS = 1000;
  }
  SequenceT.events_count = 16;
}

void initializeSequenceB(void) {
  int64_t startTime = getCurrentTimeMS() + 2000;

  for (int i = 0; i < 4; i++) {
    SequenceB.events[i].channels[0] = i;
    for (int j = 1; j < 20; j++) {
      SequenceB.events[i].channels[j] = -1;
    }
    SequenceB.events[i].startTimeMS = startTime + (i * 5000);
    SequenceB.events[i].activeTimeMS = 200;
  }
  SequenceB.events_count = 4;
}
