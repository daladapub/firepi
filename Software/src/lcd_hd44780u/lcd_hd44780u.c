#include "lcd.h"


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "pigpio.h"

#define LCD_DATA_BITSIZE    4

#define LCD_PULSE_MS 5
#define LCD_DELAY_MS 5

#define LCD_PIN_RS  1
#define LCD_PIN_RW  1
#define LCD_PIN_EN  1
#define LCD_PIN_D0   1
#define LCD_PIN_D1   1
#define LCD_PIN_D2   1
#define LCD_PIN_D3   1
#define LCD_PIN_D4   1
#define LCD_PIN_D5   1
#define LCD_PIN_D6   1
#define LCD_PIN_D7   1

#define LCD_RS_CMD  0x00
#define LCD_RS_CHR  0x01
#define LCD_RW_WRI  0x00
#define LCD_RW_REA  0x02

#define LCD_FLAG_FUNCSET      0x20

#define LCD_FLAG_DL_8BIT  0x10
#define LCD_FLAG_DL_4BIT  0x00
#define LCD_FLAG_N_1LINE  0x00
#define LCD_FLAG_N_2LINE  0x08
#define LCD_FLAG_F_SMALL  0x00
#define LCD_FLAG_F_BIG    0x04

#define LCD_FLAG_DISPLAY      0x08

#define LCD_FLAG_D_DISPLAYON    0x04
#define LCD_FLAG_D_DISPLAYOFF   0x00
#define LCD_FLAG_C_CURSOROFF    0x00
#define LCD_FLAG_C_CURSORON     0x02
#define LCD_FLAG_B_CURSORBLINK  0x01
#define LCD_FLAG_B_CURSORSTEAD  0x00

#define LCD_FLAG_CLEARDISPLAY 0x01

#define LCD_FLAG_RETURNHOME 0x02

#define LCD_FLAG_ENTRYMODE 0x04

#define LCD_FLAG_ID_TORIGHT       0x02
#define LCD_FLAG_ID_TOLEFT        0x00
#define LCD_FLAG_S_SHIFTDISPLAY   0x01
#define LCD_FLAG_S_SHIFTCURSOR    0x00

#define LCD_FLAG_SETCGRAMADDRESS  0x20

#define LCD_FLAG_SETDDRAMADDRESS  0x40

void lcd_WriteByte(char byte, char mode);


void lcd_CursorHome();

void lcd_ClearScreen() {

}

void lcd_SetDisplay(int state);
void lcd_SetCursor(int state);
void lcd_SetCursorBlink(int state);
void lcd_SendCommand(unsigned char command);
void lcd_SetPosition(int x, int y);
void lcd_DefCharDisp(int index, unsigned char data [8]);

void lcd_PutChar(unsigned char data) {
    lcd_WriteByte(data, LCD_RS_CHR);
}

void lcd_Puts(const char *string) {
    while(*string) {
        lcd_WriteByte(*string, LCD_RS_CHR);
        string++;
    }
}

void lcd_Printf(const char *message, ...) {
    va_list argp;
    char buffer[40];
    va_start(argp, message);
    vsnprintf(buffer, 40, message, argp);
    va_end(argp);

    buffer[39] = 0;
    lcd_Puts(buffer);
}

int lcd_Init(){
    gpioSetMode(LCD_PIN_RS, PI_OUTPUT);
    gpioSetMode(LCD_PIN_RW, PI_OUTPUT);
    gpioSetMode(LCD_PIN_EN, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D0, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D1, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D2, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D3, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D4, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D5, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D6, PI_OUTPUT);
    gpioSetMode(LCD_PIN_D7, PI_OUTPUT);

    lcd_WriteByte(0x33, LCD_RS_CMD);
    lcd_WriteByte(0x32, LCD_RS_CMD);
    lcd_WriteByte(0x28, LCD_RS_CMD);
    lcd_WriteByte(0x0C, LCD_RS_CMD);
    lcd_WriteByte(0x06, LCD_RS_CMD);
    lcd_WriteByte(0x01, LCD_RS_CMD);
}

void lcd_WriteByte(char byte, char mode) {
    gpioWrite(LCD_PIN_RS, 0);
    gpioWrite(LCD_PIN_RW, 0);
    gpioWrite(LCD_PIN_EN, 0);
    gpioWrite(LCD_PIN_D0, 0);
    gpioWrite(LCD_PIN_D1, 0);
    gpioWrite(LCD_PIN_D2, 0);
    gpioWrite(LCD_PIN_D3, 0);
    gpioWrite(LCD_PIN_D4, 0);
    gpioWrite(LCD_PIN_D5, 0);
    gpioWrite(LCD_PIN_D6, 0);
    gpioWrite(LCD_PIN_D7, 0);

    gpioWrite(LCD_PIN_RS, (mode>>0)&0x01);
    gpioWrite(LCD_PIN_RW, (mode>>1)&0x01);
    gpioWrite(LCD_PIN_D4, (byte>>4)&0x01);
    gpioWrite(LCD_PIN_D5, (byte>>5)&0x01);
    gpioWrite(LCD_PIN_D6, (byte>>6)&0x01);
    gpioWrite(LCD_PIN_D7, (byte>>7)&0x01);

    gpioDelay(LCD_DELAY_MS*1000l);

    gpioWrite(LCD_PIN_EN, 1);

    gpioDelay(LCD_PULSE_MS*1000l);

    gpioWrite(LCD_PIN_EN, 0);

    gpioDelay(LCD_DELAY_MS*1000l);

    gpioWrite(LCD_PIN_D4, (byte>>0)&0x01);
    gpioWrite(LCD_PIN_D5, (byte>>1)&0x01);
    gpioWrite(LCD_PIN_D6, (byte>>2)&0x01);
    gpioWrite(LCD_PIN_D7, (byte>>3)&0x01);

    gpioDelay(LCD_DELAY_MS*1000l);

    gpioWrite(LCD_PIN_EN, 1);

    gpioDelay(LCD_PULSE_MS*1000l);

    gpioWrite(LCD_PIN_EN, 0);

    gpioDelay(LCD_DELAY_MS*1000l);
}

#ifdef _LCDTEST
void main(void){
  lcd_Init();
  lcd_Puts("Hallo, Welt!");
}
#endif /*_LCDTEST*/
