#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "log.h"

#define LOGDEBUG(args...) flogf(LOG_DEBUG, logfile, args)
#define LOGINFO(args...) flogf(LOG_INFO, logfile, fargs)
#define LOGWARNING(args...) flogf(LOG_WARNING, logfile, args)
#define LOGERROR(args...) flogf(LOG_ERROR, logfile, args)
#define LOGFATAL(args...) flogf(LOG_FATAL, logfile, args)

// #define LOGDEBUG(args...) printf(args)
// #define LOGINFO(args...) printf(args)
// #define LOGWARNING(args...) printf(args)
// #define LOGERROR(args...) printf(args)
// #define LOGFATAL(args...) printf(args)





extern FILE *logfile;

#endif /*__LOGGER_H__*/
