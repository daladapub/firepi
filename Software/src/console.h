#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include "stdbool.h"
#include "stdint.h"

bool getPulseStopFlag(void);
bool getExitFlag(void);
bool getPulseStartFlag(void);
bool getPulseLoadFlag(void);
bool getPulseUnloadFlag(void);
enum Sequences getActiveSequence(void);
void setActiveSequenceStarted(void);
void setActiveSequenceEnded(void);
void console_setNextShotDueTime(int64_t duetime);

void console_Init(void);
void console_WaitInput(void);
void *console_inputController(void *param);

#endif /*__CONSOLE_H__*/
