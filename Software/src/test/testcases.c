
#include "test.h"

#include "testcases.h"
#include "stdlib.h"

TestCase_t TestCases[] = {
  {
    id: 0,
    desc: "TestCase 1",
    testfunc: &tc1_func
  },
  {
    id: 1,
    desc: "TestCase 2",
    testfunc: &tc2_func
  },
  {
    id: -1,
    desc: "",
    testfunc: NULL
  }
};
