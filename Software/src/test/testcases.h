#ifndef __TESTCASES_H__
#  define __TESTCASES_H__

#include "test.h"

TCState_t tc1_func(void);
TCState_t tc2_func(void);

#endif /*__TESTCASES_H__*/
