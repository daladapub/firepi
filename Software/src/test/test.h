#ifndef __TEST_H__
#  define __TEST_H__

enum TC_STATE { TC_NOTRUN, TC_PASSED, TC_FAILED };
typedef enum TC_STATE TCState_t;

struct TestCase_s {
  int id;
  char* desc;
  TCState_t (*testfunc)(void);
};
typedef struct TestCase_s TestCase_t;

extern TestCase_t TestCases[];

#endif /*__TEST_H__*/
