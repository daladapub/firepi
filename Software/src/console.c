#include "console.h"

#include "pthread.h"
#include "sequences.h"
#include "stdio.h"
#include "termios.h"

#define CONSOLE_EA_enter() pthread_mutex_lock(&inpAvAccess)
#define CONSOLE_EA_leave() pthread_mutex_unlock(&inpAvAccess)

typedef enum { NORMAL, SEQUSEL } InpMode_t;

volatile bool pulseStopFlag;
volatile bool exitFlag;
enum Sequences activeSequence;
bool activeSequenceStarted;
int64_t nextShotDueTime;

pthread_cond_t inputAvailable = PTHREAD_COND_INITIALIZER;
pthread_mutex_t inpAvAccess = PTHREAD_MUTEX_INITIALIZER;

void console_Init(void) {
  CONSOLE_EA_enter();
  pulseStopFlag = false;
  exitFlag = false;
  activeSequence = NO_SEQUENCE;
  activeSequenceStarted = false;
  CONSOLE_EA_leave();
}

bool getPulseStopFlag(void) {
  bool retval;
  CONSOLE_EA_enter();
  retval = pulseStopFlag;
  CONSOLE_EA_leave();
  return retval;
}

bool getExitFlag(void) {
  bool retval;
  CONSOLE_EA_enter();
  retval = exitFlag;
  CONSOLE_EA_leave();
  return retval;
}

enum Sequences getActiveSequence(void) {
  enum Sequences retval;
  CONSOLE_EA_enter();
  retval = activeSequence;
  CONSOLE_EA_leave();
  return retval;
}

void setActiveSequenceStarted(void) {
  CONSOLE_EA_enter();
  activeSequenceStarted = true;
  CONSOLE_EA_leave();
}

void setActiveSequenceEnded(void) {
  CONSOLE_EA_enter();
  activeSequenceStarted = false;
  activeSequence = NO_SEQUENCE;
  CONSOLE_EA_leave();
}

int64_t getNextShotDueTime() {
  int64_t retval;
  CONSOLE_EA_enter();
  retval = nextShotDueTime;
  CONSOLE_EA_leave();
  return retval;
}

void console_setNextShotDueTime(int64_t duetime) {
  CONSOLE_EA_enter();
  nextShotDueTime = duetime;
  CONSOLE_EA_leave();
}

void console_WaitInput(void) {
  CONSOLE_EA_enter();
  pthread_cond_wait(&inputAvailable, &inpAvAccess);
  CONSOLE_EA_leave();
}

void setPulseStopFlag(bool flag) {
  CONSOLE_EA_enter();
  pulseStopFlag = flag;
  CONSOLE_EA_leave();
}

void setExitFlag(bool flag) {
  CONSOLE_EA_enter();
  exitFlag = flag;
  CONSOLE_EA_leave();
}

void setActiveSequence(enum Sequences sequence) {
  CONSOLE_EA_enter();
  activeSequence = sequence;
  CONSOLE_EA_leave();
}

void console_notifyInput() {
  CONSOLE_EA_enter();
  pthread_cond_signal(&inputAvailable);
  CONSOLE_EA_leave();
}

#define TIME_TEILER_TAG (1000*60*60*24)
#define TIME_TEILER_STUNDE (1000*60*60)
#define TIME_TEILER_MINUTE (1000*60)
#define TIME_TEILER_SEKUNDE (1000)

int timePrettyPrint(char* buf, int64_t time) {
  int tage, stunden, minuten, sekunden;
  tage = time / TIME_TEILER_TAG;
  time = time % TIME_TEILER_TAG;
  stunden = time / TIME_TEILER_STUNDE;
  time = time % TIME_TEILER_STUNDE;
  minuten = time / TIME_TEILER_MINUTE;
  time = time % TIME_TEILER_MINUTE;
  sekunden = time / TIME_TEILER_SEKUNDE;
  return sprintf(buf, "%d day(s), %d hour(s), %d minute(s), %d second(s)", tage, stunden, minuten, sekunden);
}

static struct termios old, new;

/* Initialize new terminal i/o settings */
void initTermios(int echo) {
  tcgetattr(0, &old);     /* grab old terminal i/o settings */
  new = old;              /* make new settings same as old settings */
  new.c_lflag &= ~ICANON; /* disable buffered i/o */
  new.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &new); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) { tcsetattr(0, TCSANOW, &old); }

/* Read 1 character - echo defines echo mode */
char getch_(int echo) {
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) { return getch_(0); }

/* Read 1 character with echo */
char getche(void) { return getch_(1); }

void *console_inputController(void *param) {
  char buf[80];
  char cmd = 0;
  InpMode_t curmod = NORMAL;

  printf("FIP loader started\n");
  printf("Choose one of following options:\n");
  printf("\t(e)xit     s(t)atus   (r)un      (s)top     (h)elp\n");
  do {
    switch (curmod) {
    case NORMAL: {
      printf("\n>> ");
      cmd = getche();
      switch (cmd) {
      case 'e': {
        printf("\nExiting...\n");
        setExitFlag(true);
        console_notifyInput();
      } break;
      case 't': {
        printf("\nCurrently %s is loaded%s.",
               SequenceNames[getActiveSequence()],
               (activeSequenceStarted) ? ", and started" : "");
        if(getActiveSequence() != 0 ) {
          if(timePrettyPrint(buf, getNextShotDueTime())>0){
            printf("\nNext shot due in %s", buf);
          }
        }
      } break;
      case 'h': {
        printf("\nInput can be entered interactively.");
        printf("\nType one of following chars in brackets to alter function:");
        printf("\n    (e)xit     Exit the program. Waits for last pulse to "
               "finish");
        printf("\n   s(t)atus t   Show the current status");
        printf("\n    (r)un      Run a sequence");
        printf("\n    (s)top     Stop a running sequence immediatelly");
        printf("\n    (h)elp     Show this help message");
      } break;
      case 'r': {
        curmod = SEQUSEL;
      } break;
      case 's': {
        printf("\nStopping current Sequence (%s)",
               SequenceNames[getActiveSequence()]);
        setPulseStopFlag(true);
        setActiveSequence(NO_SEQUENCE);
      } break;
      }
      cmd = 0;
    } break;
    case SEQUSEL: {
      printf("\nSelect one of possible Sequences (A, B, C, T) or push 0 to not "
             "load a sequence:\n");
      printf("\n.. ");
      cmd = getche();
      switch (cmd) {
      case 'A':
      case 'a':
        setActiveSequence(SEQUENCE_A);
        break;
      case 'B':
      case 'b':
        setActiveSequence(SEQUENCE_B);
        break;
      case 'C':
      case 'c':
        setActiveSequence(SEQUENCE_C);
        break;
      case 'T':
      case 't':
        setActiveSequence(SEQUENCE_T);
        break;
      case '0':
        setActiveSequence(NO_SEQUENCE);
        break;
      default:
        continue;
      }
      curmod = NORMAL;
      if (getActiveSequence() != NO_SEQUENCE) {
        printf("\n%s selected. Will start Sequence now.",
               SequenceNames[getActiveSequence()]);
        setPulseStopFlag(false);
        console_notifyInput();
      } else {
        printf("\nNo Sequence selected.");
      }
    } break;
    }
  } while (!getExitFlag());
}
